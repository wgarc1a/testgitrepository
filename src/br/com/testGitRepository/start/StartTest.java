package br.com.testGitRepository.start;

import javax.annotation.Resource;

import br.com.testGitRepository.bean.Carro;
import br.com.testGitRepository.bo.CarroBO;

public class StartTest {
	
	@Resource
	private CarroBO carroBO;

	public static void main(String[] args) {
		new StartTest();
	}
	
	public StartTest(){
		this.insereCarros();
	}
	
	private void insereCarros(){
		Carro carro = new Carro();
		carro.setId(1L);
		carro.setNome("Civic");
		carro.setMarca("Honda");
		carro.setCor("Preto");
		carro.setPlaca("EFA8127");
		carro.setRenavam(256425487L);
		carro.setChassi("dsf54sd5f46sdf46sd5f");
		
		carroBO.insere(carro);
		
	}

}
